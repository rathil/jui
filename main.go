package main

import (
	"encoding/json"
	"gitlab.com/rathil/jui/src"
	"log"
	"os"
)

func main() {
	var data src.Data
	if err := json.NewDecoder(os.Stdin).Decode(&data); err != nil {
		log.Fatal(err)
	}
	jui, err := src.Jui()
	if err != nil {
		log.Fatal(err)
	}
	defer jui.Close()
	wd := src.WidgetData()
	wp := src.WidgetPath()
	jui.AddWidget(wd)
	jui.AddWidget(wp)
	//jui.AddWidget(src.WidgetControl())
	jui.AddWidget(src.WidgetInspector(wd, wp, data.TreeNode()), true)
	if err := jui.Run(); err != nil {
		log.Fatal(err)
	}
}
