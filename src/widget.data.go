package src

import (
	"github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"image"
	"strings"
)

func WidgetData() *widgetData {
	element := widgets.NewParagraph()
	element.Title = "Data"
	element.Border = true
	return &widgetData{
		Paragraph: element,
	}
}

type widgetData struct {
	*widgets.Paragraph
}

func (a *widgetData) Event(
	event termui.Event,
) {
}

func (a *widgetData) GetDrawable(
	focus bool,
	w int,
	h int,
) termui.Drawable {
	if focus {
		a.BorderStyle.Fg = termui.ColorCyan
	} else {
		a.BorderStyle.Fg = termui.ColorWhite
	}
	a.SetRect(50, 3, w, h)
	return a
}

func (a *widgetData) SetText(text string) {
	a.Text = text
}

func (a *widgetData) IsMe(e termui.Event) bool {
	if m, ok := e.Payload.(termui.Mouse); ok {
		return image.Rectangle{
			Min: image.Point{X: m.X, Y: m.Y},
			Max: image.Point{X: m.X + 1, Y: m.Y + 1},
		}.In(a.GetRect())
	}
	return false
}

func (a *widgetData) CanFocus() bool {
	return true
}

func (a *widgetData) Draw(buf *termui.Buffer) {
	a.Block.Draw(buf)

	text := strings.ReplaceAll(a.Text, "[\n", "\u0000\n")
	cells := termui.ParseStyles(text, a.TextStyle)
	for index, cell := range cells {
		if cell.Rune == '\u0000' {
			cells[index].Rune = '['
		}
	}
	if a.WrapText {
		cells = termui.WrapCells(cells, uint(a.Inner.Dx()))
	}

	rows := termui.SplitCells(cells, '\n')

	for y, row := range rows {
		if y+a.Inner.Min.Y >= a.Inner.Max.Y {
			break
		}
		row = termui.TrimCells(row, a.Inner.Dx())
		for _, cx := range termui.BuildCellWithXArray(row) {
			x, cell := cx.X, cx.Cell
			buf.SetCell(cell, image.Pt(x, y).Add(a.Inner.Min))
		}
	}
}
