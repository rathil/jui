package src

import (
	"fmt"
	"github.com/gizak/termui/v3"
	"github.com/nsf/termbox-go"
)

func Jui() (*jui, error) {
	if err := termui.Init(); err != nil {
		return nil, err
	}
	return &jui{
		focus: -1,
	}, nil
}

type jui struct {
	focus   int
	widgets []Widget
}

type Widget interface {
	Event(e termui.Event)
	GetDrawable(focus bool, w int, h int) termui.Drawable
	IsMe(e termui.Event) bool
	CanFocus() bool
}

func (a *jui) AddWidget(w Widget, focus ...bool) {
	a.widgets = append(a.widgets, w)
	if len(focus) == 1 && focus[0] {
		a.focus = len(a.widgets) - 1
	}
}

func (a *jui) Draw() {
	w, h := termbox.Size()
	items := make([]termui.Drawable, 0, len(a.widgets))
	for index, widget := range a.widgets {
		items = append(items, widget.GetDrawable(index == a.focus, w, h))
	}
	termui.Render(items...)
}

func (a *jui) Run() error {
	if a.focus == -1 {
		return fmt.Errorf(`does not specify for which widget the focus is set`)
	}
	a.Draw()
	for e := range termui.PollEvents() {
		switch e.ID {
		case "q", "<C-c>", "<F10>":
			return nil
		case "<Tab>":
			a.focus++
			if a.focus >= len(a.widgets) {
				a.focus = 0
			} else {
				for i := a.focus; i < len(a.widgets); i++ {
					if a.widgets[i].CanFocus() {
						a.focus = i
						break
					}
				}
			}
			a.Draw()
			continue
		case "<Resize>":
			a.Draw()
			continue
		case "<MouseLeft>":
			for index, widget := range a.widgets {
				if widget.IsMe(e) {
					a.focus = index
					break
				}
			}
		default:
			a.widgets[a.focus].Event(e)
		}
		a.Draw()
	}
	return nil
}

func (a *jui) Close() {
	termui.Close()
}
