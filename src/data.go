package src

import (
	"encoding/json"
	"fmt"
	"github.com/gizak/termui/v3/widgets"
)

type Data struct {
	name  string
	path  string
	data  interface{}
	nodes []*Data
}

func createData(data interface{}, path string) *Data {
	a := &Data{
		path: path,
		data: data,
	}
	switch items := data.(type) {
	case map[string]interface{}:
		a.nodes = make([]*Data, 0, len(items))
		for key, val := range items {
			node := createData(val, fmt.Sprintf(`%s[["%s"]]()`, path, key))
			node.name = key
			a.nodes = append(a.nodes, node)
		}
	case []interface{}:
		a.nodes = make([]*Data, 0, len(items))
		for key, val := range items {
			name := fmt.Sprintf("[[%d]]()", key)
			node := createData(val, path+name)
			node.name = name
			a.nodes = append(a.nodes, node)
		}
	}
	return a
}

func (a *Data) MarshalJSON() ([]byte, error) {
	return json.Marshal(a.data)
}

func (a *Data) UnmarshalJSON(data []byte) error {
	if err := json.Unmarshal(data, &a.data); err != nil {
		return err
	}
	*a = *createData(a.data, "")
	a.name = "Root"
	return nil
}

func (a *Data) String() string {
	return a.name
}

func (a *Data) Data() interface{} {
	return a.data
}

func (a *Data) Path() string {
	return a.path
}

func (a *Data) TreeNode() *widgets.TreeNode {
	res := &widgets.TreeNode{
		Value:    a,
		Expanded: true,
		Nodes:    make([]*widgets.TreeNode, 0, len(a.nodes)),
	}
	for _, node := range a.nodes {
		res.Nodes = append(res.Nodes, node.TreeNode())
	}
	return res
}
