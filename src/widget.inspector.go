package src

import (
	"github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"github.com/hokaccha/go-prettyjson"
	"image"
)

func WidgetInspector(printerData printer, printerPath printer, nodes ...*widgets.TreeNode) *widgetInspector {
	element := widgets.NewTree()
	element.Title = "Inspector"
	element.SelectedRow = 0
	element.SelectedRowStyle.Bg = termui.ColorCyan
	element.SetNodes(nodes)
	element.Border = true

	f := prettyjson.NewFormatter()
	f.KeyColor.Color = termui.ColorCyan
	f.KeyColor.Mod = termui.ModifierClear
	f.StringColor.Mod = termui.ModifierClear
	f.StringColor.Color = termui.ColorGreen
	f.BoolColor.Mod = termui.ModifierClear
	f.NumberColor.Mod = termui.ModifierClear
	f.NumberColor.Color = termui.ColorWhite
	f.NullColor.Mod = termui.ModifierClear
	f.NullColor.Color = termui.ColorYellow

	a := &widgetInspector{
		Tree:        element,
		printerData: printerData,
		printerPath: printerPath,
		formatter:   f,
	}
	a.updateData()
	return a
}

type printer interface {
	SetText(text string)
}

type widgetInspector struct {
	*widgets.Tree
	printerData printer
	printerPath printer
	formatter   *prettyjson.Formatter
}

func (a *widgetInspector) updateData() {
	raw, _ := a.formatter.Marshal(a.SelectedNode().Value.(*Data).Data())
	a.printerData.SetText(string(raw))
}

func (a *widgetInspector) updatePath() {
	a.printerPath.SetText(a.SelectedNode().Value.(*Data).Path())
}

func (a *widgetInspector) Event(
	e termui.Event,
) {
	open := func() {
		if a.SelectedNode().Expanded {
			var expand func(node *widgets.TreeNode)
			expand = func(node *widgets.TreeNode) {
				node.Expanded = true
				for _, node := range node.Nodes {
					expand(node)
				}
			}
			expand(a.SelectedNode())
		}
		a.Expand()
	}
	close := func() {
		if !a.SelectedNode().Expanded {
			return
		}
		a.Collapse()
		var collapse func(node *widgets.TreeNode)
		collapse = func(node *widgets.TreeNode) {
			node.Expanded = false
			for _, node := range node.Nodes {
				collapse(node)
			}
		}
		collapse(a.SelectedNode())
	}
	switch e.ID {
	case "<Up>":
		a.ScrollUp()
		a.updateData()
		a.updatePath()
	case "<Down>":
		a.ScrollDown()
		a.updateData()
		a.updatePath()
	case "<PageUp>":
		a.ScrollPageUp()
		a.updateData()
		a.updatePath()
	case "<PageDown>":
		a.ScrollPageDown()
		a.updateData()
		a.updatePath()
	case "<Home>":
		a.ScrollTop()
		a.updateData()
		a.updatePath()
	case "<End>":
		a.ScrollBottom()
		a.updateData()
		a.updatePath()
	case "<Space>":
		if a.SelectedNode().Expanded {
			close()
		} else {
			open()
		}
	case "<Left>":
		close()
	case "<Right>":
		open()
	}
}

func (a *widgetInspector) GetDrawable(
	focus bool,
	w int,
	h int,
) termui.Drawable {
	if focus {
		a.BorderStyle.Fg = termui.ColorCyan
	} else {
		a.BorderStyle.Fg = termui.ColorWhite
	}
	a.SetRect(0, 0, 50, h)
	return a
}

func (a *widgetInspector) IsMe(e termui.Event) bool {
	if m, ok := e.Payload.(termui.Mouse); ok {
		return image.Rectangle{
			Min: image.Point{X: m.X, Y: m.Y},
			Max: image.Point{X: m.X + 1, Y: m.Y + 1},
		}.In(a.GetRect())
	}
	return false
}

func (a *widgetInspector) CanFocus() bool {
	return true
}
