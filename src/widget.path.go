package src

import (
	"github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"image"
)

func WidgetPath() *widgetPath {
	element := widgets.NewParagraph()
	element.Title = "Path"
	element.TextStyle.Bg = termui.ColorBlue
	element.Border = true
	return &widgetPath{
		Paragraph: element,
	}
}

type widgetPath struct {
	*widgets.Paragraph
}

func (a *widgetPath) Event(
	event termui.Event,
) {
}

func (a *widgetPath) GetDrawable(
	focus bool,
	w int,
	h int,
) termui.Drawable {
	if focus {
		a.BorderStyle.Fg = termui.ColorCyan
	} else {
		a.BorderStyle.Fg = termui.ColorWhite
	}
	a.SetRect(50, 0, w, 3)
	return a
}

func (a *widgetPath) SetText(text string) {
	a.Text = text
}

func (a *widgetPath) IsMe(e termui.Event) bool {
	if m, ok := e.Payload.(termui.Mouse); ok {
		return image.Rectangle{
			Min: image.Point{X: m.X, Y: m.Y},
			Max: image.Point{X: m.X + 1, Y: m.Y + 1},
		}.In(a.GetRect())
	}
	return false
}

func (a *widgetPath) CanFocus() bool {
	return true
}
