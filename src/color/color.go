package color

import (
	"fmt"
	"github.com/gizak/termui/v3"
)

var colorMap = map[termui.Color]string{
	termui.ColorRed:     "red",
	termui.ColorBlue:    "blue",
	termui.ColorBlack:   "black",
	termui.ColorCyan:    "cyan",
	termui.ColorYellow:  "yellow",
	termui.ColorWhite:   "white",
	termui.ColorClear:   "clear",
	termui.ColorGreen:   "green",
	termui.ColorMagenta: "magenta",
}

var modMap = map[termui.Modifier]string{
	termui.ModifierBold:      "bold",
	termui.ModifierUnderline: "underline",
	termui.ModifierReverse:   "reverse",
}

type Color struct {
	Color termui.Color
	Mod   termui.Modifier
}

const (
	Bold      = termui.ModifierBold
	Underline = termui.ModifierUnderline

	FgBlack   = termui.ColorBlack
	FgGreen   = termui.ColorGreen
	FgYellow  = termui.ColorYellow
	FgBlue    = termui.ColorBlue
	FgMagenta = termui.ColorMagenta
	FgCyan    = termui.ColorCyan
)

func New(
	color termui.Color,
	mod termui.Modifier,
) *Color {
	return &Color{
		Color: color,
		Mod:   mod,
	}
}

func (a *Color) SprintFunc() func(args ...interface{}) string {
	return func(args ...interface{}) string {
		return fmt.Sprintf(`[%s](fg:%s,mod:%s)`, fmt.Sprint(args...), colorMap[a.Color], modMap[a.Mod])
	}
}

func (a *Color) SprintfFunc() func(format string, args ...interface{}) string {
	return func(format string, args ...interface{}) string {
		return fmt.Sprintf(`[%s](fg:%s,mod:%s)`, fmt.Sprintf(format, args...), colorMap[a.Color], modMap[a.Mod])
	}
}
