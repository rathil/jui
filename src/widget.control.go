package src

import (
	"github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

func WidgetControl() *widgetControl {
	element := widgets.NewParagraph()
	element.Text = "Data 222"
	element.Border = false
	return &widgetControl{
		Paragraph: element,
	}
}

type widgetControl struct {
	*widgets.Paragraph
}

func (a *widgetControl) Event(
	event termui.Event,
) {}

func (a *widgetControl) GetDrawable(
	focus bool,
	w int,
	h int,
) termui.Drawable {
	if focus {
		a.BorderStyle.Fg = termui.ColorCyan
	} else {
		a.BorderStyle.Fg = termui.ColorWhite
	}
	a.SetRect(0, h-1, w, h)
	return a
}

func (a *widgetControl) IsMe(e termui.Event) bool {
	return false
}

func (a *widgetControl) CanFocus() bool {
	return false
}
