module gitlab.com/rathil/jui

go 1.16

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/gizak/termui/v3 v3.1.0
	github.com/hokaccha/go-prettyjson v0.0.0-20210113012101-fb4e108d2519 // indirect
	github.com/nsf/termbox-go v0.0.0-20190121233118-02980233997d
)

replace (
	github.com/fatih/color => ./src/color
)